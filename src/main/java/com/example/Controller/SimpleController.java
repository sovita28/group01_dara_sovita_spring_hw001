package com.example.Controller;

import com.example.model.Customer;
import com.example.model.CustomerUser;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class SimpleController {
    List<Customer> customers = new ArrayList<>();
    public SimpleController(){
        customers.add(new Customer("sovita", "male", 20, "pp"));
        customers.add(new Customer("sonisa", "female", 20, "ksp"));
        customers.add(new Customer("gogo", "male", 23, "kt"));
        customers.add(new Customer("papa", "female", 24, "kp"));
    }

    @PostMapping ("/addCustomer")
    public ResponseEntity<?> addCustomer(@RequestBody CustomerUser cus){
        HashMap<String,Object> reponses = new HashMap();

        Customer c = new Customer();
        c.setId(Customer.auto++);
        c.setName(cus.getName());
        c.setGender(cus.getGender());
        c.setAge(cus.getAge());
        c.setAddress(cus.getAddress());
        customers.add(c);

        reponses.put("Status ", "Add successfully.");
        reponses.put("Data ", c);

        return new ResponseEntity<>(reponses, HttpStatus.OK);
    }
    @GetMapping("/getCustomer")
    public List<Customer> getCustomer(){
        return customers;
    }
    @GetMapping("/getById/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable int id) {
        HashMap<String,Object> reponses = new HashMap();

        for(Customer ct : customers){
            if(ct.getId() == id){
                reponses.put("status", "Get successfully");
                reponses.put("data", ct);
                return new ResponseEntity<>(reponses,HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("ID Not Found !!",HttpStatus.NOT_FOUND);
    }
    @GetMapping("/getByName")
    public ResponseEntity<?> getCustomerByName(@RequestParam String name) {
        HashMap<String,Object> reponses = new HashMap();

        for(Customer ct : customers){
            if(ct.getName().equals(name)){
                reponses.put("status", "Get successfully");
                reponses.put("data", ct);
                return new ResponseEntity<>(reponses,HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("Name Not Found !!",HttpStatus.NOT_FOUND);
    }
    @PutMapping("/updateCustomerById/{id}")
    public ResponseEntity<?> updateCustomerById(@PathVariable int id, @RequestBody CustomerUser cus){
        HashMap<String,Object> reponses = new HashMap();

        Customer c = new Customer();
        c.setId(id);
        c.setName(cus.getName());
        c.setGender(cus.getGender());
        c.setAge(cus.getAge());
        c.setAddress(cus.getAddress());

        for(int i=0; i<customers.size(); i++){
            if(id == customers.get(i).getId()){
                customers.set(id-1, c);

                reponses.put("status", "Updated successfully");
                reponses.put("data", c);
                return new ResponseEntity<>(reponses,HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("ID Not Found !!",HttpStatus.NOT_FOUND);
    }
    @DeleteMapping("/remove/{id}")
    public ResponseEntity<?> removeCustomerById(@PathVariable int id){
        HashMap<String,Object> reponses = new HashMap();
        for(int i=0; i<=customers.size(); i++){
            if(id == customers.get(i).getId()){
                reponses.put("stutus", "removed Successfully");
                reponses.put("data", customers.get(i));
                customers.remove(i);
                return new ResponseEntity<>(reponses,HttpStatus.OK);
            }
        }
        return null;
    }
}
