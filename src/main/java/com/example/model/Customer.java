package com.example.model;

import com.example.Controller.SimpleController;
import nonapi.io.github.classgraph.json.Id;

public class Customer{
    private int id;
    private String name;
    private String gender;
    private int age;
    private String address;

    public static int auto = 1;

    CustomerUser customerUser;

    public Customer() {
    }

    public Customer(String name, String gender, int age, String address) {
        this.id = auto++;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
